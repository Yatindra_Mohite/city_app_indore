<?php 
ob_start();
include('header.php');
include('sidebarmenu.php');
include('include/dbconnection.php');

if(isset($_POST['add_category'])) 
{

  $category_name=$_POST['category_name'];



   $image= ($_FILES['file']['tmp_name']);
   $image_name= ($_FILES['file']['name']);

   $date1 = date('d-m-y h:i:s');
   echo $image_name;
   $image_name = $date1.$image_name;
    
   move_uploaded_file($_FILES["file"]['tmp_name'],"images/category_image/".$image_name);
            
            $location="images/category_image/".$image_name;
  
  $add = mysqli_query($conn,"INSERT INTO `smily_category`(`category_name`,`category_image`) VALUES('$category_name','$location')");

  if($add)
        {
        
           header('location:smile.php');

       	}
    
}



?>

      <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
        <!-- page start-->
	
		
        <div class="row">
			<div class="col-sm-12">
                <section class="panel">
                    	<!-- 	<a href="#myModal-1" data-toggle="modal" class="btn btn-danger">Add Category</a> -->
                            
                           <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal-1" class="modal fade">
                            <div class="modal-dialog" style="margin: 100px auto;">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                        <h4 class="modal-title">Add New Category</h4>
                                    </div>
                                    <div class="modal-body">

                                       <form class="" role="form" action="smile.php" method="post" enctype="multipart/form-data">
                                            <div class="form-group" style="">
                                               <lable>Name</lable>
                                                <input type="text" name="category_name" class="form-control" placeholder="Enter Name" required>
                                            </div>  
                                            
                                            <div class="form-group" style="">
                                               <lable>Category Image</lable> 
                                                <input type="file" name="file" class="form-control" >
                                            </div>
				                                   
				                            <div class="form-group" style="">                                       
                                                <button type="submit" name="add_category" class="btn btn-primary" style="">Add</button>
                                        	</div>
                                        </form>

                                    </div>

                                </div>
                            </div>
                        </div>
					 
                    <div class="panel-body">
      <div class="col-sm-12">
           
					 
                   
                <div class="panel-body">
                        <section id="unseen">
                            <table class="table table-bordered table-striped table-condensed">
                                <thead>
                                <tr>
                                    <th>S No.</th>
                                     <th>Category Name</th> 
                                     <th>Category Image</th>
                              		<th>Action</th>  
																
                                </tr>
                                </thead>
                                <tbody>
                               
								<?php
								$result1 = mysqli_query($conn,"SELECT * FROM `smily_category` order by category_id desc");
								if(mysqli_num_rows($result1)>0)
								{
								$sno=1;
								while($rowselect = mysqli_fetch_array($result1))
								{
									$id=$rowselect['category_id'];
									
								echo"<tr id='trrow".$rowselect['category_id']."'>
									<td style='width:1%;'>".$sno."</td>
										<td>
									<label >".$rowselect['category_name']."</label>
										</td>
						
										<td>
									<label ><img src=".$rowselect['category_image']." width='100' height='50'></label>
										</td>

									
										
										<td style=''>
										<a id='add".$rowselect['category_id']."' class='btn btn-info' href='gallery_add.php?id=".$rowselect['category_id']."' title='add image' data-rel='tooltip'  >Add Image
										</a>

										<a id='edit".$rowselect['category_id']."' class='btn btn-info' href='gallery_show.php?id=".$rowselect['category_id']."' title='Edit' data-rel='tooltip'  >Show Image
										<i class='fa fa-edit'></i> </a>

										<!--<a id='delete".$rowselect['category_id']."' onclick='deletemain(\"".$rowselect['category_id']."\");' class='btn btn-danger' href='#' title='Delete' data-rel='tooltip'  >
											<i class='fa fa-trash-o '></i>  
										</a>-->
									
										</td>
									</tr>";
								$sno++;
							}
						}
								?>                                                                
                                                          
                               
                                
                                </tbody>
                            </table>
                        </section>
                    </div>
               
            </div>
        </div>
        <!-- page end-->
        </section>
    </section>
	
	<script>
	function editmain(data)
	{
		$('#lname'+data).hide();
		$('#edit'+data).hide();
		$('#delete'+data).hide();
		
		$('#tname'+data).show();
		$('#save'+data).show();
		

	}
	
	function savemain(data)
	{
		document.getElementById('lname'+data).innerHTML=$('#tname'+data).val();
		
		$('#tname'+data).hide();
		$('#lname'+data).show();
		
		$('#save'+data).hide();
		$('#edit'+data).show();
		$('#delete'+data).show();
		
		var name = $('#tname'+data).val();
		$.ajax({
				url:'update.php?status=3&id='+data+'&name='+name,
				success:function(data){
				
				}
			});
	}
	
	function deletemain(data)
	{
		var r = confirm('Are you really want to delete this category ?');
		if(r==true)
		{
			$('#trrow'+data).hide();
			$.ajax({
				url:'delete.php?status=10&id='+data,
				success:function(data){
				
				}
			});
		}
	}
	
	</script>
	
</section>
<!-- Placed js at the end of the document so the pages load faster -->
<!--Core js-->
<script src="js/jquery.js"></script>
<script src="js/jquery-ui/jquery-ui-1.10.1.custom.min.js"></script>
<script src="bs3/js/bootstrap.min.js"></script>
<script src="js/jquery.dcjqaccordion.2.7.js"></script>
<script src="js/jquery.scrollTo.min.js"></script>
<script src="js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="js/jquery.nicescroll.js"></script>
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/flot-chart/excanvas.min.js"></script><![endif]-->
<script src="js/skycons/skycons.js"></script>
<script src="js/jquery.scrollTo/jquery.scrollTo.js"></script>
<script src="jquery.easing.min.js"></script>
<script src="js/calendar/clndr.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min.js"></script>
<script src="js/calendar/moment-2.2.1.js"></script>
<script src="js/evnt.calendar.init.js"></script>
<script src="js/jvector-map/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/jvector-map/jquery-jvectormap-us-lcc-en.js"></script>
<script src="js/gauge/gauge.js"></script>
<!--clock init-->
<script src="js/css3clock/js/css3clock.js"></script>
<!--Easy Pie Chart-->
<script src="js/easypiechart/jquery.easypiechart.js"></script>
<!--Sparkline Chart-->
<script src="js/sparkline/jquery.sparkline.js"></script>
<!--Morris Chart-->
<script src="js/morris-chart/morris.js"></script>
<script src="js/morris-chart/raphael-min.js"></script>
<!--jQuery Flot Chart-->
<script src="js/flot-chart/jquery.flot.js"></script>
<script src="js/flot-chart/jquery.flot.tooltip.min.js"></script>
<script src="js/flot-chart/jquery.flot.resize.js"></script>
<script src="js/flot-chart/jquery.flot.pie.resize.js"></script>
<script src="js/flot-chart/jquery.flot.animator.min.js"></script>
<script src="js/flot-chart/jquery.flot.growraf.js"></script>
<script src="js/dashboard.js"></script>
<script src="js/jquery.customSelect.min.js" ></script>
<!--common script init for all pages-->
<script src="js/scripts.js"></script>
<!--script for this page-->
<script>
function select(data)
	{
	
				var staff= $('#staff'+data).val();
			
				if(staff!= '')
				{
					
					$.ajax({
						type:'GET',
						url:'update.php?status=11&staff='+staff,
						success:function(data){
							$('#position').empty();
							$('#position').append('<option value="" >SELECT</option>');
							$('#position').append(data);
							
						}
					});
				} 
	
	}
</script>
</body>

</html>