<?php 
//ini_set( "display_errors", 0);
include('include/dbconnection.php');
 session_start();
  if(isset($_SESSION['login_user']))
    {
      header('location:home.php');
    }

if(isset($_POST['submit']))
{
    $username=$_POST['username'];
    $password=$_POST['password'];
    $ch="SELECT * FROM `user` WHERE `username`='$username' AND `password`='$password'";
    $c=mysqli_query($conn,$ch);
    if(mysqli_num_rows($c)>0)
    {
      $_SESSION['login_user']='username';
      header('location:home.php');
    }
    else
    {
        echo"<script>alert('username and password not matched');</script>";
    }
   
  }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="images/favicon.png">

    <title>Login</title>

    <!--Core CSS -->
    <link href="bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />

    
</head>

  <body class="login-body">

    <div class="container">
<?php

//session_destroy();
?>
      <form class="form-signin" action="index.php" method="post">
        <h2 class="form-signin-heading">sign in now</h2>
        <div class="login-wrap">
            <div class="user-login-info">
                <input type="text" name="username" class="form-control" placeholder="User Name" autofocus required>
                <input type="password" name="password" class="form-control" placeholder="Password" required>
            </div>
            <label class="checkbox">
                <input type="checkbox" value="remember-me"> Remember me
                <span class="pull-right">
                   <?php /*?> <a data-toggle="modal" href="#myModal"> Forgot Password?</a><?php */?>

                </span>
            </label>
            <button class="btn btn-lg btn-login btn-block" name="submit" type="submit">Sign in</button>

            <div class="registration">
                
                <a class="" href="registration.php">
                    
                </a>
            </div>

        </div>
    </form>
           
          <form class="form-signin" action="forget.php" method="post"> 
		<!-- <form class="form-signin" action="#" method="post">-->
          <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
              <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                          <h4 class="modal-title">Forgot Password ?</h4>
                      </div>
                      <div class="modal-body">
                          <p>Enter your e-mail address below to reset your password.</p>
                          <input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">

                      </div>
                      <div class="modal-footer">
                          <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                          <button class="btn btn-success" name="btnlogin" type="submit">Submit</button>
                      </div>
					  
					  
                  </div>
              </div>
          </div>
          <!-- modal -->

      </form>

	  
	  
	  
    </div>



    <!-- Placed js at the end of the document so the pages load faster -->

    <!--Core js-->
    <script src="js/jquery.js"></script>
    <script src="bs3/js/bootstrap.min.js"></script>

  </body>
</html>
