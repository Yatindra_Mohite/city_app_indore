<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/plug-ins/1.10.6/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

<?php 
ob_start();
error_reporting(0);
include('header.php');
include('sidebarmenu.php');
include('include/dbconnection.php');

if(isset($_POST['add_new_job'])) 
{

	$name = $_POST['name'];
	$email = $_POST['email'];
	$mobile = $_POST['mobile'];
	$location = $_POST['location'];
	$job_title = $_POST['job_title'];
	$experience = $_POST['experience'];
	$experience1 = $_POST['experience1'];
	$company_name = $_POST['company_name'];
	$address = $_POST['address'];
	$ctc = $_POST['ctc'];
	$valid_till = $_POST['valid_till'];
	$tag = $_POST['tag'];
	$post_date = date("d-M-Y");

  $a="INSERT INTO `job_post`(`name`,`email`,`mobile`,`area`,`job_title`,`experience`,`speciality`,`company_name`,`location`,`address`,`ctc`,`post_date`,`valid_till`,`status`)VALUES('$name','$email','$mobile','$experience1','$job_title','$experience','$tag','$company_name','$location','$address','$ctc','$post_date','$valid_till','Active')";
  $ad    =    mysqli_query($conn,$a);

        if($ad)
        {   
        	$id=mysqli_insert_id($conn);
        	$query1 = mysqli_query($conn,"SELECT * FROM `user`");
           
           if(mysqli_num_rows($query1)>0) 
           {
                $gcmRegIds = array();
                $i = 0;
                while($query_row = mysqli_fetch_assoc($query1)) 
                {
                      $i++;
                       $gcmRegIds[floor($i/1000)][] = $query_row['device_token'];
                }
              
              $pushMessage= array("id" =>$id, "name" =>$name, "email" =>$email, "mobile" =>$mobile, "location" =>$location,"job_title" =>$job_title, "table_key" =>"job_post", "experience" =>$experience, "address" =>$address, "ctc" =>$ctc, "valid_till" =>$valid_till, "tag" =>$tag, "company_name" =>$company_name, "post_date" =>$post_date);
              
              if(isset($gcmRegIds)) 
              {
                  $message = $pushMessage;
                  $pushStatus = array();
                  foreach($gcmRegIds as $val) $pushStatus[] = sendPushNotification($val, $message);
              }

           } header('location:job_post.php');

       	}else
		{
		echo"Failed";
		}
}

function sendPushNotification($registration_ids, $message)
{
  //print_r($message);exit;
    $url = 'https://android.googleapis.com/gcm/send';
    $fields = array(
        'registration_ids' => $registration_ids,
        'data' => array("message"=>$message),
    );

    define('GOOGLE_API_KEY', 'AIzaSyB--dodvnt6cba-sBz4XKZ3s6T-HjlyuVA' );

    $headers = array(
        'Authorization:key=' . GOOGLE_API_KEY,
        'Content-Type: application/json'
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

     $result = curl_exec($ch);
  //  if($result === false)
       // die('Curl failed ' . curl_error());

    curl_close($ch);
    return $result;
}


?>

      <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
        <!-- page start-->
	
		
        <div class="row">
			<div class="col-sm-12">
			JOB POST
                <section class="panel">
                    
					 
                 <div class="panel-body">
      				<div class="col-sm-12">
           		    &nbsp&nbsp&nbsp&nbsp&nbsp<a href="#myModal-1" data-toggle="modal" class="btn btn-danger">Add New Detail</a>
                            
                           <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal-1" class="modal fade">
                            <div class="modal-dialog" style="margin: 100px auto;">
                                <div class="modal-content" style="height: 600px;">
                                    <div class="modal-header">
                                       <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                        <h4 class="modal-title">Add New </h4>
                                    </div>
                                    <div class="modal-body">

                                       <form class="" role="form" action="job_post.php" method="post" enctype="multipart/form-data">
                                            
                                           <div class="col-md-6" style="">
                                               <lable>For</lable>
                                               <select name="experience1" class="form-control" required>
                                               	<option value="">Select</option>
                                               	<option value="1">Fresher</option>
                                               	<option value="2">Experience</option>
                                    
                                               </select>
                                            </div>

                                            <div class="col-md-6" style="">
                                               <lable>Category</lable>
                                               <select name="tag" class="form-control" required>
                                               <option value="">Select</option>
                                               <?php $sele = mysqli_query($conn,"SELECT * FROM `speciality` where cate_id = 'job_post'");
													 while($res3=mysqli_fetch_array($sele))
													{
													echo ("<option value=".$res3['id'].">".$res3['name']."</option>");
													}
												?> 	
         
                                               </select>
                                            </div>

                               
                                            
                                             <div class="col-md-6" style="">
                                               <lable>Email</lable>
                                                <input type="email"  name="email" class="form-control" placeholder="" >
                                            </div>

                                            

                                            <div class="col-md-6" style="">
                                               <lable>Mobile</lable>
                                                <input type="text"  name="mobile" class="form-control" placeholder="" >
                                            </div>

                                            <div class="col-md-6" style="">
                                               <lable>Job Title</lable>
                                                <input type="text" name="job_title" class="form-control" placeholder="" >
                                            </div>

                                             <div class="col-md-6" style="">
                                               <lable>Company Name</lable>
                                                <input type="text" name="company_name" class="form-control" placeholder="" required>
                                            </div>
                                             <div class="col-md-6" style="">
                                               <lable>CTC</lable>
                                                <input type="text" name="ctc" class="form-control" placeholder="" >
                                            </div>
                                             <div class="col-md-6" style="">
                                               <lable>Valid Till</lable>
                                                <input type="date" name="valid_till" class="form-control" placeholder="" >
                                            </div>
                                             <div class="col-md-6" style="">
                                               <lable>Location</lable>
                                                <input type="text" name="location" class="form-control" placeholder="" >
                                            </div>
                                            <div class="col-md-6" style="">
                                               <lable>Add Experience</lable>
                                               <input type='number'  name="experience" class="form-control">
                                            </div>
											<div class="col-md-12" style="">
                                               <lable>Description</lable>
                                               <textarea  name="name" class="form-control"></textarea> 
                                            </div> 

                                            <div class="col-md-12" style="">
                                                 <lable>Address</lable>
                                                 <textarea  name="address" class="form-control"></textarea> 
                                            </div>
				                                   
				                            <div class="col-md-12" > 
                                                <button type="submit" name="add_new_job" style="margin-top: 5%;" class="btn btn-primary" style="">Add</button>
                                        	</div>
                                        </form>

                                    </div>

                                </div>
                            </div>
                        </div>
					 
                   
<div class="panel-body">
    <section id="unseen">
    	<div class="table-responsive">
        <table id="example_my" class="table table-bordered table-striped table-condensed">
            <thead>
            <tr class='success'>
            	 <th>Sno.</th>
                 <th>Job Title</th> 
                 <th>Description</th> 
                 <th>Company Name</th> 
                 <th>Email</th> 
          		 <th>Location</th> 
          		 <th>CTC</th> 
          		 <th>mobile</th> 
                 <th>Action</th>  
											
            </tr>
            </thead>
            <tbody>
           
			<?php
			$result1 = mysqli_query($conn,"SELECT * FROM job_post order by id desc");
			if(mysqli_num_rows($result1)>0)
			{
			$sno=1;
			while($rowselect = mysqli_fetch_array($result1))
			{
				$id=$rowselect['id'];
				
			echo"<tr id='trrow".$rowselect['id']."'>
					<td >".$sno."</td>
					<td>
				<label >".$rowselect['job_title']."</label>
					</td>

					<td>
				<label >".$rowselect['name']."</label>
					</td>
					<td>
				<label >".$rowselect['company_name']."</label>
					</td>
		
					<td >
				<label >".$rowselect['email']."</label>
					</td>

					<td>
				<label >".$rowselect['location']."</label>
					</td>
		
					<td>
				<label >".$rowselect['ctc']."</label>
					</td>

					<td>
				<label >".$rowselect['mobile']."</label>
					</td>
					<td>
				<label >".$rowselect['status']."</label>
					</td>

					
					<td style=''>
				
					<a id='edit".$rowselect['id']."' target='_blank' class='btn btn-info' href='job_post_update.php?id=".$rowselect['id']."' title='Edit' data-rel='tooltip'>
					Edit</a>
					
					<a id='delete".$rowselect['id']."' onclick='deletemain(\"".$rowselect['id']."\");' class='btn btn-danger' href='#' title='Delete' data-rel='tooltip'>
					<i class='fa fa-trash-o '></i>  
					</a>
				
					</td>
				</tr>";
			$sno++;
		}
	}
			?>                                                                
        
            	</tbody>
        	</table>
    	</div>
   </section>
</div>
        </div>
      	  </div>
        <!-- page end-->
        	</section>
    				</section>
	
	<script>
	function editmain(data)
	{
		$('#lname'+data).hide();
		$('#edit'+data).hide();
		$('#delete'+data).hide();
		
		$('#tname'+data).show();
		$('#save'+data).show();
		

	}
	
	function savemain(data)
	{
		document.getElementById('lname'+data).innerHTML=$('#tname'+data).val();
		
		$('#tname'+data).hide();
		$('#lname'+data).show();
		
		$('#save'+data).hide();
		$('#edit'+data).show();
		$('#delete'+data).show();
		
		var name = $('#tname'+data).val();
		$.ajax({
				url:'update.php?status=3&id='+data+'&name='+name,
				success:function(data){
				
				}
			});
	}
	
	function deletemain(data)
	{
		var r = confirm('Are you really want to delete this Job Post ?');
		if(r==true)
		{
			$('#trrow'+data).hide();
			$.ajax({
				url:'delete.php?status=12&id='+data,
				success:function(data){
				
				}
			});
		}
	}
	
	</script>

	
</section>


  <script src="https://code.jquery.com/jquery-1.11.1.min.js" type="text/javascript"></script>
  
  <script src="https://cdn.datatables.net/1.10.6/js/jquery.dataTables.min.js" type="text/javascript"></script>
  <script src="https://cdn.datatables.net/plug-ins/1.10.6/integration/bootstrap/3/dataTables.bootstrap.js" type="text/javascript"></script>

<script>
$(document).ready(function() {
    $('#example_my').dataTable();
} );

</script>
<!-- Placed js at the end of the document so the pages load faster -->
<!--Core js-->
<script src="bs3/js/bootstrap.min.js"></script>
<script src="js/jquery.scrollTo/jquery.scrollTo.js"></script>
<script src="js/dashboard.js"></script>
<script src="js/jquery.customSelect.min.js" ></script>

<script src="js/scripts.js"></script>
<!--script for this page-->
<script>
function select(data)
	{
	
				var staff= $('#staff'+data).val();
			
				if(staff!= '')
				{
					
					$.ajax({
						type:'GET',
						url:'update.php?status=11&staff='+staff,
						success:function(data){
							$('#position').empty();
							$('#position').append('<option value="" >SELECT</option>');
							$('#position').append(data);
							
						}
					});
				} 
	
	}
</script>
</body>

</html>