<?php 
include('header.php');
include('sidebarmenu.php');
include('include/dbconnection.php');
?>

      <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
        <!-- page start-->
	
		
        <div class="row">
			<div class="col-sm-12">
                <section class="panel">
                    
					 
                    <div class="panel-body">
      <div class="col-sm-12">
           
					 
                   
                <div class="panel-body">
                        <section id="unseen">
                            <table class="table table-bordered table-striped table-condensed">
                                <thead>
                                <tr>
                                    <th>S No.</th>
                                     <th>Name</th>
                                     <th>Email</th>  
                                     <th>Address</th>  
                                      <th>Gender</th>  
                              		<th>Action</th>  
																
                                </tr>
                                </thead>
                                <tbody>
                               
								<?php
								$result = mysqli_query($conn,"SELECT * FROM user order by id desc");
								$sno=1;
								while($rowselect = mysqli_fetch_array($result))
								{
									 
									
								echo"<tr id='trrow".$rowselect['id']."'>
									<td style='width:70px;'>".$sno."</td>
										<td>
		<label id='lalb_name".$rowselect['id']."'>".$rowselect['name']."</label>
		<input type='text' id='talb_name".$rowselect['id']."' value='".$rowselect['name']."' style='display:none;width:80px' />
										</td>
										
										
										<td>
									<label >".$rowselect['email']."</label>
										</td>
										
										
										<td>
									<label >".$rowselect['address']."</label>
										</td>

										<td>
									<label >".$rowselect['sex']."</label>
										</td>
										
										<td class='center'>
										<a id='delete".$rowselect['id']."' onclick='deletemain(\"".$rowselect['id']."\");' class='btn btn-danger' href='#' title='Delete' data-rel='tooltip'  >
											<i class='fa fa-trash-o '></i>  
										</a>
										
											
										</td>
									</tr>";
								$sno++;
							}
								?>                                                                
                                                          
                               
                                
                                </tbody>
                            </table>
                        </section>
                    </div>
               
            </div>
        </div>
        <!-- page end-->
        </section>
    </section>
	
	<script>
	function editmain(data)
	{
		$('#lname'+data).hide();
		$('#edit'+data).hide();
		$('#delete'+data).hide();
		
		$('#tname'+data).show();
		$('#save'+data).show();
		

	}
	
	function savemain(data)
	{
		document.getElementById('lname'+data).innerHTML=$('#tname'+data).val();
		
		$('#tname'+data).hide();
		$('#lname'+data).show();
		
		$('#save'+data).hide();
		$('#edit'+data).show();
		$('#delete'+data).show();
		
		var name = $('#tname'+data).val();
		$.ajax({
				url:'update.php?status=3&id='+data+'&name='+name,
				success:function(data){
				
				}
			});
	}
	
	function deletemain(data)
	{
		var r = confirm('Are you really want to delete this user ?');
		if(r==true)
		{
			$('#trrow'+data).hide();
			$.ajax({
				url:'delete2.php?status=4&id='+data,
				success:function(data){
				
				}
			});
		}
	}
	
	</script>
	
</section>
<!-- Placed js at the end of the document so the pages load faster -->
<!--Core js-->
<script src="js/jquery.js"></script>
<script src="js/jquery-ui/jquery-ui-1.10.1.custom.min.js"></script>
<script src="bs3/js/bootstrap.min.js"></script>
<script src="js/jquery.dcjqaccordion.2.7.js"></script>
<script src="js/jquery.scrollTo.min.js"></script>
<script src="js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="js/jquery.nicescroll.js"></script>
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/flot-chart/excanvas.min.js"></script><![endif]-->
<script src="js/skycons/skycons.js"></script>
<script src="js/jquery.scrollTo/jquery.scrollTo.js"></script>
<script src="jquery.easing.min.js"></script>
<script src="js/calendar/clndr.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min.js"></script>
<script src="js/calendar/moment-2.2.1.js"></script>
<script src="js/evnt.calendar.init.js"></script>
<script src="js/jvector-map/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/jvector-map/jquery-jvectormap-us-lcc-en.js"></script>
<script src="js/gauge/gauge.js"></script>
<!--clock init-->
<script src="js/css3clock/js/css3clock.js"></script>
<!--Easy Pie Chart-->
<script src="js/easypiechart/jquery.easypiechart.js"></script>
<!--Sparkline Chart-->
<script src="js/sparkline/jquery.sparkline.js"></script>
<!--Morris Chart-->
<script src="js/morris-chart/morris.js"></script>
<script src="js/morris-chart/raphael-min.js"></script>
<!--jQuery Flot Chart-->
<script src="js/flot-chart/jquery.flot.js"></script>
<script src="js/flot-chart/jquery.flot.tooltip.min.js"></script>
<script src="js/flot-chart/jquery.flot.resize.js"></script>
<script src="js/flot-chart/jquery.flot.pie.resize.js"></script>
<script src="js/flot-chart/jquery.flot.animator.min.js"></script>
<script src="js/flot-chart/jquery.flot.growraf.js"></script>
<script src="js/dashboard.js"></script>
<script src="js/jquery.customSelect.min.js" ></script>
<!--common script init for all pages-->
<script src="js/scripts.js"></script>
<!--script for this page-->
<script>
function select(data)
	{
	
				var staff= $('#staff'+data).val();
			
				if(staff!= '')
				{
					
					$.ajax({
						type:'GET',
						url:'update.php?status=11&staff='+staff,
						success:function(data){
							$('#position').empty();
							$('#position').append('<option value="" >SELECT</option>');
							$('#position').append(data);
							
						}
					});
				} 
	
	}
</script>
</body>

</html>